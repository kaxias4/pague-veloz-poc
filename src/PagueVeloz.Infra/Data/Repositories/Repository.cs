using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PagueVeloz.Domain.Interfaces.Infra.Data;

namespace PagueVeloz.Infra.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region Properties

        protected readonly DbContext _dbContext;
        protected readonly DbSet<TEntity> _dbSet;

        #endregion

        #region Constructor

        public Repository(DbContext context)
        {
            _dbContext = context;
            _dbSet = _dbContext.Set<TEntity>();
        }

        #endregion

        #region Methods

        public IQueryable<TEntity> Query(string sql, params object[] parameters) =>
            _dbSet.FromSqlRaw(sql, parameters);

        public int Count(Expression<Func<TEntity, bool>> predicate = null)
        {
            IQueryable<TEntity> query = _dbSet;
            query = query.AsNoTracking();

            if (predicate != null)
                query = query.Where(predicate);

            return query.Count();
        }

        public async Task<TEntity> SingleAsync(
            Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> include = null,
            bool disableTracking = true)
        {
            IQueryable<TEntity> query = _dbSet;

            if (disableTracking)
                query = query.AsNoTracking();

            if (include != null)
                query = include(query);

            if (predicate != null)
                query = query.Where(predicate);

            if (orderBy != null)
                return await orderBy(query).FirstOrDefaultAsync();

            return await query.FirstOrDefaultAsync();
        }

        public async Task<IList<TEntity>> GetListAsync(
            Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> include = null,
            int index = 0,
            int size = 20,
            bool disableTracking = true)
        {
            IQueryable<TEntity> query = _dbSet;

            if (disableTracking)
                query = query.AsNoTracking();

            if (include != null)
                query = include(query);

            if (predicate != null)
                query = query.Where(predicate);

            if (orderBy != null)
                return await orderBy(query).Skip(index * size).Take(size).ToListAsync();
            else
                return await query.Skip(index * size).Take(size).ToListAsync();
        }

        public void Add(TEntity entity) => _dbSet.Add(entity);

        public void Update(TEntity entity) => _dbSet.Update(entity);

        public void Delete(TEntity entity)
        {
            var existing = _dbSet.Find(entity);
            if (existing != null)
                _dbSet.Remove(existing);
        }

        public void Dispose() => _dbContext?.Dispose();

        #endregion
    }
}