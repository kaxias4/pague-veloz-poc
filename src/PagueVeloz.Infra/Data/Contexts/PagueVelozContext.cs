using Microsoft.EntityFrameworkCore;
using PagueVeloz.Domain.Interfaces.Infra.Data;
using PagueVeloz.Infra.Data.Configurations;

namespace PagueVeloz.Infra.Data.Contexts
{
    public class PagueVelozContext : DbContext
    {
        public PagueVelozContext(DbContextOptions options) : base(options)
        {
        }

        #region Methods

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CompanyConfiguration());
            modelBuilder.ApplyConfiguration(new PhoneConfiguration());
            modelBuilder.ApplyConfiguration(new IndividualSupplierConfiguration());
            modelBuilder.ApplyConfiguration(new LegalSupplierConfiguration());
            modelBuilder.ApplyConfiguration(new SupplierConfiguration());
        }

        #endregion
    }
}