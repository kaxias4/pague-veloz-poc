using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using PagueVeloz.Domain.Interfaces;
using PagueVeloz.Domain.Interfaces.Infra.Data;
using PagueVeloz.Infra.Data.Contexts;
using PagueVeloz.Infra.Data.Repositories;

namespace PagueVeloz.Infra.Data
{
    public class UnitOfWork<TContext> : IUnitOfWork<TContext>, IUnitOfWork
        where TContext : DbContext, IDisposable
    {
        #region Properties

        public TContext Context { get; }

        private Dictionary<Type, object> _repositories;

        #endregion

        #region Constructors

        public UnitOfWork(TContext context) =>
            Context = context ?? throw new ArgumentNullException(nameof(context));


        #endregion

        #region Methods

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            if (_repositories == null)
                _repositories = new Dictionary<Type, object>();

            var type = typeof(TEntity);
            if (!_repositories.ContainsKey(type))
                _repositories[type] = new Repository<TEntity>(Context);

            return (IRepository<TEntity>)_repositories[type];
        }

        public int SaveChanges() => Context.SaveChanges();

        public void Dispose() => Context?.Dispose();

        #endregion
    }
}