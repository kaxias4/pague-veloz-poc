using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PagueVeloz.Domain.Entities;

namespace PagueVeloz.Infra.Data.Configurations
{
    public class PhoneConfiguration : IEntityTypeConfiguration<Phone>
    {
        public void Configure(EntityTypeBuilder<Phone> builder)
        {
            builder.ToTable("phones");

            builder.Property(e => e.Id)
                .HasColumnName("id")
                .IsRequired();
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Number)
                .HasColumnName("number")
                .IsRequired();

            builder.Property(e => e.Type)
                .HasColumnName("type")
                .HasConversion<string>()
                .IsRequired();
        }
    }
}