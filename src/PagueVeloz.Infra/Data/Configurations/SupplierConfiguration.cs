using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PagueVeloz.Domain.Entities;

namespace PagueVeloz.Infra.Data.Configurations
{
    public class SupplierConfiguration : IEntityTypeConfiguration<Supplier>
    {
        public void Configure(EntityTypeBuilder<Supplier> builder)
        {
            builder.ToTable("suppliers");

            builder.Property(e => e.Id)
                .HasColumnName("id")
                .IsRequired();
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Name)
                .HasColumnName("name")
                .IsRequired();

            builder.HasOne(e => e.Company)
                .WithMany()
                .HasConstraintName("FK_supplier_company")
                .HasForeignKey("company_id")
                .IsRequired();

            builder.HasMany<Phone>("_phones")
                .WithOne()
                .HasConstraintName("FK_phone_supplier")
                .HasForeignKey("supplier_id")
                .IsRequired();
        }
    }
}