using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PagueVeloz.Domain.Entities;

namespace PagueVeloz.Infra.Data.Configurations
{
    public class IndividualSupplierConfiguration : IEntityTypeConfiguration<IndividualSupplier>
    {
        public void Configure(EntityTypeBuilder<IndividualSupplier> builder)
        {
            builder.ToTable("individual_suppliers");

            builder.Property(e => e.CPF)
                .HasColumnName("cpf")
                .HasMaxLength(11);

            builder.Property(e => e.RG)
                .HasColumnName("rg")
                .HasMaxLength(10);

            builder.Property(e => e.DateOfBirth)
                .HasColumnName("date_of_birth");
        }
    }
}