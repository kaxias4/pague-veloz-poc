using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PagueVeloz.Domain.Entities;

namespace PagueVeloz.Infra.Data.Configurations
{
    public class LegalSupplierConfiguration : IEntityTypeConfiguration<LegalSupplier>
    {
        public void Configure(EntityTypeBuilder<LegalSupplier> builder)
        {
            builder.ToTable("legal_suppliers");

            builder.Property(e => e.CNPJ)
                .HasColumnName("cnpj")
                .HasMaxLength(14);
        }
    }
}