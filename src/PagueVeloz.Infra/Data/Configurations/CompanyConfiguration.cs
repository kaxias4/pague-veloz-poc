using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PagueVeloz.Domain.Entities;

namespace PagueVeloz.Infra.Data.Configurations
{
    public class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.ToTable("companies");

            builder.Property(e => e.Id)
                .HasColumnName("id")
                .IsRequired();
            builder.HasKey(e => e.Id);

            builder.Property(e => e.TradingName)
                .HasColumnName("trading_name")
                .IsRequired();

            builder.Property(e => e.CNPJ)
                .HasColumnName("cnpj")
                .HasMaxLength(14)
                .IsRequired();

            builder.Property(e => e.UF)
                .HasColumnName("uf")
                .HasMaxLength(2)
                .IsRequired();

        }
    }
}