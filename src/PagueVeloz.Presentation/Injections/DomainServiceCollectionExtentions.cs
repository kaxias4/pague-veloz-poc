using Microsoft.Extensions.DependencyInjection;
using PagueVeloz.Domain.Interfaces.Services;
using PagueVeloz.Domain.Services;

namespace PagueVeloz.Presentation.Injections
{
    public static class DomainServiceCollectionExtentions
    {
        public static IServiceCollection AddDomainServices(this IServiceCollection services)
        {

            services.AddScoped(typeof(IService<>), typeof(Service<>));
            services.AddScoped<ICompanyService, CompanyService>();
            services.AddScoped<IIndividualSupplierService, IndividualSupplierService>();
            services.AddScoped<ILegalSupplierService, LegalSupplierService>();

            return services;

        }
    }
}