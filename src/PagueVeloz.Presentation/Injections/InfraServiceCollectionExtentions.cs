using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PagueVeloz.Domain.Interfaces.Infra.Data;
using PagueVeloz.Infra.Data;

namespace PagueVeloz.Presentation.Injections
{

    public static class InfraServiceCollectionExtentions
    {

        public static IServiceCollection AddUnitOfWork<TContext>(this IServiceCollection services)
            where TContext : DbContext
        {

            services.AddScoped<IUnitOfWork, UnitOfWork<TContext>>();
            services.AddScoped<IUnitOfWork<TContext>, UnitOfWork<TContext>>();
            return services;

        }

    }
}