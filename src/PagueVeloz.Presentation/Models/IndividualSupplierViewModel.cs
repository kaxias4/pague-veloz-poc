using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using PagueVeloz.Domain.Entities;
using PagueVeloz.Presentation.Attributes;

namespace PagueVeloz.Presentation.Models
{

    public class IndividualSupplierDetailViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Type { get; set; }
        public string Company { get; set; }
        public List<string> Phones { get; set; }
    }

    public class IndividualSupplierCreateViewModel
    {

        #region Create

        [Required(ErrorMessage = "Nome é obrigatório.")]
        public string Name { get; set; }

        [CPF]
        public string CPF { get; set; }

        public string RG { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        public int CompanyId { get; set; }

        public List<PhoneItem> Phones { get; set; } = new List<PhoneItem>();

        #endregion

        #region View
        public IEnumerable<SelectListItem> Companies { get; set; } = new List<SelectListItem>();

        #endregion

        #region Constructors

        public IndividualSupplierCreateViewModel()
        {
        }

        #endregion
    }

    public class IndividualSupplierEditViewModel
    {

        #region Edit

        public int Id { get; set; }

        [Required(ErrorMessage = "Nome é obrigatório.")]
        public string Name { get; set; }

        [CPF]
        public string CPF { get; set; }

        public string RG { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        public int CompanyId { get; set; }

        public List<PhoneItem> Phones = new List<PhoneItem>();

        #endregion

        #region View
        public IEnumerable<SelectListItem> Companies { get; set; } = new List<SelectListItem>();

        #endregion

        #region Constructors

        public IndividualSupplierEditViewModel()
        {
        }

        #endregion
    }

}