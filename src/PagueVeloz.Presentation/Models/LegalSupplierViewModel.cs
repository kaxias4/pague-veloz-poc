using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using PagueVeloz.Domain.Entities;
using PagueVeloz.Presentation.Attributes;

namespace PagueVeloz.Presentation.Models
{
    public class LegalSupplierDetailViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CNPJ { get; set; }
        public string Type { get; set; }
        public string Company { get; set; }
        public List<string> Phones { get; set; }
    }

    public class LegalSupplierCreateViewModel
    {

        #region Create

        [Required(ErrorMessage = "Nome é obrigatório.")]
        public string Name { get; set; }

        [CNPJ]
        public string CNPJ { get; set; }

        public int Type { get; set; }

        public int CompanyId { get; set; }

        public List<PhoneItem> Phones = new List<PhoneItem>();

        #endregion

        #region View

        public IEnumerable<SelectListItem> TypeItens { get; set; } = new List<SelectListItem>();
        public IEnumerable<SelectListItem> Companies { get; set; } = new List<SelectListItem>();

        #endregion

        #region Constructors

        public LegalSupplierCreateViewModel()
        {
        }

        #endregion
    }

    public class LegalSupplierEditViewModel
    {

        #region Create

        public int Id { get; set; }

        [Required(ErrorMessage = "Nome é obrigatório.")]
        public string Name { get; set; }

        [CNPJ]
        public string CNPJ { get; set; }

        public int Type { get; set; }
        public int CompanyId { get; set; }
        public List<PhoneItem> Phones = new List<PhoneItem>();

        #endregion

        #region View

        public IEnumerable<SelectListItem> TypeItens { get; set; } = new List<SelectListItem>();
        public IEnumerable<SelectListItem> Companies { get; set; } = new List<SelectListItem>();

        #endregion

        #region Constructors

        public LegalSupplierEditViewModel()
        {
        }

        #endregion
    }

}