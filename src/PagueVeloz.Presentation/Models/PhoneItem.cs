using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using PagueVeloz.Domain.Enums;

namespace PagueVeloz.Presentation.Models
{
    public class PhoneItem
    {
        public int Id { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Number { get; set; }

        public int Type { get; set; }

        public List<SelectListItem> PhoneTypes =
            Enum.GetValues(typeof(PhoneType))
            .Cast<PhoneType>()
            .Select(p => new SelectListItem()
            {
                Text = p.ToString(),
                Value = ((int)p).ToString()
            }).ToList();
    }
}