using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using PagueVeloz.Domain.Entities;
using PagueVeloz.Presentation.Attributes;

namespace PagueVeloz.Presentation.Models
{
    public class SupplierIndexViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CNPJ_CPF { get; set; }
        public string Type { get; set; }
    }
}