using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using PagueVeloz.Domain.Enums;
using PagueVeloz.Presentation.Attributes;

namespace PagueVeloz.Presentation.Models
{

    public class CompanyIndexViewModel
    {
        public int Id { get; set; }
        public string TradingName { get; set; }
        public string CNPJ { get; set; }
    }

    public class CompanyDetailViewModel
    {
        public int Id { get; set; }
        public string TradingName { get; set; }
        public string CNPJ { get; set; }
        public string UF { get; set; }
    }

    public class CompanyCreateViewModel
    {

        #region Create

        [Required(ErrorMessage = "Nome fantasia é obrigatório.")]
        public string TradingName { get; set; }

        [Required(ErrorMessage = "CNPJ é obrigatório.")]
        [CNPJ]
        public string CNPJ { get; set; }

        [Required(ErrorMessage = "Uf é obrigatório.")]
        public string UF { get; set; }

        #endregion

        #region View

        public IEnumerable<SelectListItem> States { get; } = new List<SelectListItem>();

        #endregion

        #region Constructors

        public CompanyCreateViewModel()
        {
            States = State.GetAll().Select(s => new SelectListItem()
            {
                Text = s.Name,
                Value = s.Initials
            });
        }

        #endregion
    }

    public class CompanyEditViewModel
    {
        #region Edit

        public int Id { get; set; }

        [Required(ErrorMessage = "Nome fantasia é obrigatório.")]
        public string TradingName { get; set; }

        [Required(ErrorMessage = "CNPJ é obrigatório.")]
        [CNPJ]
        public string CNPJ { get; set; }

        [Required(ErrorMessage = "Uf é obrigatório.")]
        public string UF { get; set; }

        #endregion

        #region View

        public IEnumerable<SelectListItem> States { get; } = new List<SelectListItem>();

        #endregion

        #region Constructors

        public CompanyEditViewModel()
        {
            States = State.GetAll().Select(s => new SelectListItem()
            {
                Text = s.Name,
                Value = s.Initials
            });
        }

        #endregion
    }

}