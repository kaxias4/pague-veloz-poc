using System.ComponentModel.DataAnnotations;
using PagueVeloz.Domain.Extensions;

namespace PagueVeloz.Presentation.Attributes
{
    public class CPFAttribute : ValidationAttribute
    {

        public CPFAttribute() { }

        public string GetErrorMessage() =>
            $"CPF inválido.";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {

            if (value == null)
                return ValidationResult.Success;

            var cpf = (string)value;

            if (!cpf.IsCpf())
                return new ValidationResult(GetErrorMessage());

            return ValidationResult.Success;
        }
    }
}