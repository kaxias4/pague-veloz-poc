using System.ComponentModel.DataAnnotations;
using PagueVeloz.Domain.Extensions;

namespace PagueVeloz.Presentation.Attributes
{
    public class CNPJAttribute : ValidationAttribute
    {

        public CNPJAttribute() { }

        public string GetErrorMessage() =>
            $"CNPJ inválido.";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {

            if (value == null)
                return ValidationResult.Success;

            var cnpj = (string)value;

            if (!cnpj.IsCnpj())
                return new ValidationResult(GetErrorMessage());

            return ValidationResult.Success;
        }
    }
}