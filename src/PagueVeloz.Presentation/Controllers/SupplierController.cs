using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PagueVeloz.Domain.Entities;
using PagueVeloz.Domain.Interfaces.Services;
using PagueVeloz.Presentation.Models;

namespace PagueVeloz.Presentation.Controllers
{
    public class SupplierController : Controller
    {
        #region Properties

        private readonly IIndividualSupplierService _individualSupplierService;
        private readonly ILegalSupplierService _legalSupplierService;

        #endregion

        #region Constructors

        public SupplierController(IIndividualSupplierService individualSupplierService,
            ILegalSupplierService legalSupplierService)
        {
            _individualSupplierService = individualSupplierService;
            _legalSupplierService = legalSupplierService;
        }

        #endregion

        #region Methods

        // GET: Supplier
        public async Task<IActionResult> Index()
        {
            var suppliers = (await _individualSupplierService
                .GetListAsync())
                .Select(s =>
                    new SupplierIndexViewModel()
                    {
                        Id = s.Id,
                        Name = s.Name,
                        CNPJ_CPF = s.CPF,
                        Type = "Pessoa Física"
                    }
                ).ToList();

            suppliers.AddRange((await _legalSupplierService
                .GetListAsync())
                .Select(s =>
                    new SupplierIndexViewModel()
                    {
                        Id = s.Id,
                        Name = s.Name,
                        CNPJ_CPF = s.CNPJ,
                        Type = "Pessoa Jurídica"
                    }
                ).ToList());

            suppliers = suppliers.OrderBy(s => s.Name).ToList();

            return View(suppliers);
        }

        #endregion
    }
}