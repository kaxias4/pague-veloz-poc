using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PagueVeloz.Domain.Entities;
using PagueVeloz.Domain.Enums;
using PagueVeloz.Domain.Interfaces.Services;
using PagueVeloz.Presentation.Models;

namespace PagueVeloz.Presentation.Controllers
{
    public class IndividualSupplierController : Controller
    {

        private readonly IIndividualSupplierService _individualSupplierService;
        private readonly ICompanyService _companyService;

        public IndividualSupplierController(
            IIndividualSupplierService individualSupplierService,
            ICompanyService companyService)
        {
            _individualSupplierService = individualSupplierService;
            _companyService = companyService;
        }


        //GET: IndividualSupplier/Detail/5
        public async Task<IActionResult> Detail(int id)
        {

            if (id == 0)
                return NotFound();

            Func<IQueryable<IndividualSupplier>, IQueryable<IndividualSupplier>> includes =
                query => query.Include("_phones")
                    .Include("Company");

            var supplier = (await _individualSupplierService
                .SingleAsync(c => c.Id == id, include: includes));

            if (supplier == null)
                return NotFound();

            var model = new IndividualSupplierDetailViewModel()
            {
                Id = supplier.Id,
                Name = supplier.Name,
                CPF = supplier.CPF,
                RG = supplier.RG,
                DateOfBirth = supplier.DateOfBirth,
                Phones = supplier.Phones?.Select(p => $"{p.Number} ({p.Type.ToString()})").ToList(),
                Company = supplier.Company.TradingName,
                Type = "Pessoa Física"
            };

            return View(model);
        }

        // GET: IndividualSupplier/Create
        public async Task<IActionResult> Create()
        {
            var model = new IndividualSupplierCreateViewModel();

            model.Companies = (await _companyService.GetListAsync())
                .Select(c => new SelectListItem()
                {
                    Text = c.TradingName,
                    Value = c.Id.ToString()
                });

            return View(model);
        }

        // POST: IndividualSupplier/Create
        [HttpPost]
        public async Task<IActionResult> Create(IndividualSupplierCreateViewModel model)
        {
            model.Companies = (await _companyService.GetListAsync())
               .Select(c => new SelectListItem()
               {
                   Text = c.TradingName,
                   Value = c.Id.ToString()
               });

            try
            {

                if (!ModelState.IsValid)
                    return View(model);

                var company = await _companyService.SingleAsync(c => c.Id == model.CompanyId, disableTracking: false);

                var individualSupplier = new IndividualSupplier(
                    model.Name,
                    model.CPF,
                    model.RG,
                    model.DateOfBirth,
                    company,
                    model.Phones
                        .Select(p =>
                            new Phone(
                                p.Number,
                                (PhoneType)p.Type
                            )
                        ).ToList()
                );

                _individualSupplierService.Add(individualSupplier);

                return RedirectToAction("Detail", new { id = individualSupplier.Id });

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }
        }

        // GET: IndividualSupplier/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            Func<IQueryable<IndividualSupplier>, IQueryable<IndividualSupplier>> includes =
                query => query.Include("_phones")
                    .Include("Company");

            var supplier = (await _individualSupplierService
                .SingleAsync(c => c.Id == id, include: includes));

            if (supplier == null)
                return NotFound();

            var companies = (await _companyService.GetListAsync())
                .Select(c => new SelectListItem()
                {
                    Text = c.TradingName,
                    Value = c.Id.ToString()
                });

            var types = new List<SelectListItem>()
            {
                new SelectListItem("Física", "1"),
                new SelectListItem("Jurídica", "2")
            };

            var model = new IndividualSupplierEditViewModel()
            {
                Id = supplier.Id,
                Name = supplier.Name,
                CPF = supplier.CPF,
                RG = supplier.RG,
                DateOfBirth = supplier.DateOfBirth,
                Phones = supplier.Phones
                    .Select(p => new PhoneItem()
                    {
                        Id = p.Id,
                        Number = p.Number,
                        Type = (int)p.Type
                    }).ToList(),
                CompanyId = supplier.Company.Id,
                Companies = companies
            };

            model.Companies.FirstOrDefault(i => i.Value == model.CompanyId.ToString()).Selected = true;

            return View(model);
        }

        // POST: IndividualSupplier/Edit/5
        [HttpPost]
        public async Task<IActionResult> Edit(int id, IndividualSupplierEditViewModel model)
        {
            if (id != model.Id)
                return NotFound();

            var companies = (await _companyService.GetListAsync())
                .Select(c => new SelectListItem()
                {
                    Text = c.TradingName,
                    Value = c.Id.ToString()
                });

            model.Companies = companies;

            try
            {

                if (!ModelState.IsValid)
                    return View(model);

                Func<IQueryable<IndividualSupplier>, IQueryable<IndividualSupplier>> includes =
                    query => query.Include("_phones")
                        .Include("Company");

                var supplierStored = await _individualSupplierService
                    .SingleAsync(s => s.Id == id,
                        include: includes,
                        disableTracking: false);

                if (supplierStored == null) return NotFound();

                var company = await _companyService.SingleAsync(c => c.Id == model.CompanyId, disableTracking: false);

                supplierStored.ChangeCPF(model.CPF);
                supplierStored.ChangeName(model.Name);
                supplierStored.ChangeRG(model.RG);
                supplierStored.DateOfBirth = model.DateOfBirth;

                model.Phones.Where(p => p.Id == 0).ToList().ForEach(p =>
                {
                    var newPhone = new Phone(p.Number, (PhoneType)p.Type);
                    supplierStored.AddPhone(newPhone);
                });

                model.Phones.Where(p => p.Id != 0).ToList().ForEach(p =>
                {
                    var phoneToUpdate = supplierStored.Phones
                        .FirstOrDefault(ph => ph.Id == p.Id);

                    phoneToUpdate.ChangeNumber(p.Number);
                    phoneToUpdate.ChangePhoneType((PhoneType)p.Type);
                });

                var removePhones = supplierStored.Phones
                    .Where(p =>
                        !model.Phones.Any(x => x.Id == p.Id))
                    .ToList();

                removePhones.ForEach(p => supplierStored.RemovePhone(p));

                supplierStored.ChangeCompany(company);

                _individualSupplierService.Update(supplierStored);

                return RedirectToAction("Detail", new { id = supplierStored.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }

        }

        [HttpPost]
        public IActionResult AddPhone(IndividualSupplierCreateViewModel model)
        {
            model.Phones.Add(new PhoneItem());
            return PartialView("PhoneList", model);
        }
    }
}