using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PagueVeloz.Domain.Entities;
using PagueVeloz.Domain.Interfaces.Services;
using PagueVeloz.Presentation.Models;

namespace PagueVeloz.Presentation.Controllers
{
    public class CompanyController : Controller
    {

        #region Properties

        private readonly ICompanyService _companyService;

        #endregion

        #region Constructors

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        #endregion

        #region Methods

        // GET: Company
        public async Task<IActionResult> Index()
        {
            var companies = (await _companyService
                .GetListAsync())
                .Select(c =>
                    new CompanyIndexViewModel()
                    {
                        Id = c.Id,
                        TradingName = c.TradingName,
                        CNPJ = c.CNPJ
                    }
                );

            return View(companies);
        }

        //GET: Detail/5
        public async Task<IActionResult> Detail(int id)
        {
            if (id == 0)
                return NotFound();

            var company = (await _companyService
                .SingleAsync(c => c.Id == id));

            if (company == null)
                return NotFound();

            var model = new CompanyDetailViewModel()
            {
                Id = company.Id,
                TradingName = company.TradingName,
                CNPJ = company.CNPJ,
                UF = company.UF
            };

            return View(model);
        }

        // GET: Company/Create
        public IActionResult Create() => View(new CompanyCreateViewModel());

        // POST: Company/Create
        [HttpPost]
        public IActionResult Create(CompanyCreateViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                    return View(model);

                var newCompany = new Company(model.TradingName, model.CNPJ, model.UF);
                _companyService.Add(newCompany);

                return RedirectToAction("Detail", new { id = newCompany.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }
        }

        // GET: Company/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var company = await _companyService.SingleAsync(c => c.Id == id);
            if (company == null)
                return NotFound();

            var model = new CompanyEditViewModel()
            {
                Id = company.Id,
                TradingName = company.TradingName,
                CNPJ = company.CNPJ,
                UF = company.UF
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, CompanyEditViewModel model)
        {

            if (id != model.Id)
                return NotFound();

            try
            {

                if (!ModelState.IsValid)
                    return View(model);

                var companyStored = await _companyService.SingleAsync(c => c.Id == id);

                if (companyStored == null) return NotFound();

                companyStored.ChangeCNPJ(model.CNPJ);
                companyStored.ChangeTrandigName(model.TradingName);
                companyStored.ChangeUf(model.UF);

                _companyService.Update(companyStored);

                return RedirectToAction("Detail", new { id = companyStored.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }
        }
        #endregion
    }
}