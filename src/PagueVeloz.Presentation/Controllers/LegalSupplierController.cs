using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PagueVeloz.Domain.Entities;
using PagueVeloz.Domain.Enums;
using PagueVeloz.Domain.Interfaces.Services;
using PagueVeloz.Presentation.Models;

namespace PagueVeloz.Presentation.Controllers
{
    public class LegalSupplierController : Controller
    {

        private readonly ILegalSupplierService _legalSupplierService;
        private readonly ICompanyService _companyService;

        public LegalSupplierController(
            ILegalSupplierService legalSupplierService,
            ICompanyService companyService)
        {
            _legalSupplierService = legalSupplierService;
            _companyService = companyService;
        }

        //GET: LegalSupplier/Detail/5
        public async Task<IActionResult> Detail(int id)
        {
            if (id == 0)
                return NotFound();

            Func<IQueryable<LegalSupplier>, IQueryable<LegalSupplier>> includes =
                query => query.Include("_phones")
                    .Include("Company");

            var legalSupplier = (await _legalSupplierService
                .SingleAsync(c => c.Id == id, include: includes));

            if (legalSupplier == null)
                return NotFound();

            var model = new LegalSupplierDetailViewModel()
            {
                Id = legalSupplier.Id,
                Name = legalSupplier.Name,
                CNPJ = legalSupplier.CNPJ,
                Phones = legalSupplier.Phones.Select(p => $"{p.Number}({p.Type.ToString()})").ToList(),
                Company = legalSupplier.Company.TradingName,
                Type = "Pessoa Jurídica"
            };

            return View(model);
        }

        // GET: LegalSupplier/Create
        public async Task<IActionResult> Create()
        {
            var model = new LegalSupplierCreateViewModel();

            model.Companies = (await _companyService.GetListAsync())
                .Select(c => new SelectListItem()
                {
                    Text = c.TradingName,
                    Value = c.Id.ToString()
                });

            model.TypeItens = new List<SelectListItem>()
            {
                new SelectListItem("Física", "1"),
                new SelectListItem("Jurídica", "2")
            };

            return View(model);
        }

        // POST: LegalSupplier/Create
        [HttpPost]
        public async Task<IActionResult> Create(LegalSupplierCreateViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                    return View(model);

                var company = await _companyService.SingleAsync(c => c.Id == model.CompanyId, disableTracking: false);

                var legalSupplier = new LegalSupplier(
                    model.Name,
                    model.CNPJ,
                    company,
                    model.Phones
                        .Select(p =>
                            new Phone(
                                p.Number,
                                (PhoneType)p.Type
                            )
                        ).ToList()
                );

                _legalSupplierService.Add(legalSupplier);

                return RedirectToAction("Detail", new { id = legalSupplier.Id });

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }
        }

        // GET: LegalSupplier/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            Func<IQueryable<LegalSupplier>, IQueryable<LegalSupplier>> includes =
                query => query.Include("_phones")
                    .Include("Company");

            var supplier = (await _legalSupplierService
                .SingleAsync(c => c.Id == id, include: includes));

            if (supplier == null)
                return NotFound();

            var companies = (await _companyService.GetListAsync())
                .Select(c => new SelectListItem()
                {
                    Text = c.TradingName,
                    Value = c.Id.ToString()
                });

            var types = new List<SelectListItem>()
            {
                new SelectListItem("Física", "1"),
                new SelectListItem("Jurídica", "2")
            };

            var model = new LegalSupplierEditViewModel()
            {
                Id = supplier.Id,
                Name = supplier.Name,
                CNPJ = supplier.CNPJ,
                Phones = supplier.Phones
                    .Select(p => new PhoneItem()
                    {
                        Id = p.Id,
                        Number = p.Number,
                        Type = (int)p.Type
                    }).ToList(),
                CompanyId = supplier.Company.Id,
                Type = 2,
                Companies = companies,
                TypeItens = types
            };

            model.TypeItens.FirstOrDefault(i => i.Value == model.Type.ToString()).Selected = true;
            model.Companies.FirstOrDefault(i => i.Value == model.CompanyId.ToString()).Selected = true;

            return View(model);

        }

        // POST: IndividualSupplier/Edit/5
        [HttpPost]
        public async Task<IActionResult> Edit(int id, LegalSupplierEditViewModel model)
        {
            if (id != model.Id)
                return NotFound();

            try
            {

                if (!ModelState.IsValid)
                    return View(model);

                var supplierStored = await _legalSupplierService.SingleAsync(s => s.Id == id);

                if (supplierStored == null) return NotFound();

                var company = await _companyService.SingleAsync(c => c.Id == model.CompanyId);

                supplierStored.ChangeCNPJ(model.CNPJ);
                supplierStored.ChangeName(model.Name);

                model.Phones.Where(p => p.Id == 0).ToList().ForEach(p =>
                {
                    var newPhone = new Phone(p.Number, (PhoneType)p.Type);
                    supplierStored.AddPhone(newPhone);
                });

                model.Phones.Where(p => p.Id != 0).ToList().ForEach(p =>
                {
                    var phoneToUpdate = supplierStored.Phones
                        .FirstOrDefault(ph => ph.Id == p.Id);

                    phoneToUpdate.ChangeNumber(p.Number);
                    phoneToUpdate.ChangePhoneType((PhoneType)p.Type);
                });

                var removePhones = supplierStored.Phones
                    .Where(p =>
                        !model.Phones.Any(x => x.Id == p.Id))
                    .ToList();

                removePhones.ForEach(p => supplierStored.RemovePhone(p));

                supplierStored.ChangeCompany(company);

                _legalSupplierService.Update(supplierStored);

                return RedirectToAction("Detail", new { id = supplierStored.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }
        }

        [HttpPost]
        public IActionResult AddPhone(LegalSupplierCreateViewModel model)
        {
            model.Phones.Add(new PhoneItem());
            return PartialView("PhoneList", model);
        }

    }
}