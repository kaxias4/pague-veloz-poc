namespace PagueVeloz.Domain.Interfaces {
    public interface IEntity {
        int Id { get; }
    }
}