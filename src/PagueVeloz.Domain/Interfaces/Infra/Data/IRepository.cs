using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using PagueVeloz.Domain.Interfaces;

namespace PagueVeloz.Domain.Interfaces.Infra.Data
{
    public interface IRepository<TEntity> : IRepository, IDisposable where TEntity : class
    {

        IQueryable<TEntity> Query(string sql, params object[] parameters);
        int Count(Expression<Func<TEntity, bool>> predicate = null);

        Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> include = null,
            bool disableTracking = true);

        Task<IList<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> include = null,
            int index = 0,
            int size = 20,
            bool disableTracking = true);

        void Add(TEntity entity);

        void Delete(TEntity entity);

        void Update(TEntity entity);

    }

    public interface IRepository
    {

    }

}