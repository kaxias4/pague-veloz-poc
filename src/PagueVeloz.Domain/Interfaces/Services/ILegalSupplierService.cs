using PagueVeloz.Domain.Entities;

namespace PagueVeloz.Domain.Interfaces.Services
{
    public interface ILegalSupplierService : IService<LegalSupplier>
    {

    }
}