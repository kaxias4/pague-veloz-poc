using PagueVeloz.Domain.Entities;

namespace PagueVeloz.Domain.Interfaces.Services
{
    public interface ICompanyService : IService<Company>
    {

    }
}