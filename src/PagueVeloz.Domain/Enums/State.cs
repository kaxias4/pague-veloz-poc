using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PagueVeloz.Domain.Enums {

    public class State : IComparable {

        #region Enumaration

        public static State Acre = new State ("AC", "Acre");
        public static State Alagoas = new State ("AL", "Alagoas");
        public static State Amapa = new State ("AP", "Amapá");
        public static State Amazonas = new State ("AM", "Amazonas");
        public static State Bahia = new State ("BA", "Bahia");
        public static State Ceara = new State ("CE", "Ceará");
        public static State DistritoFederal = new State ("DF", "Distrito Federal");
        public static State EspiritoSanto = new State ("ES", "Espírito Santo");
        public static State Goias = new State ("GO", "Goias");
        public static State Maranhao = new State ("MA", "Maranhão");
        public static State MatoGrosso = new State ("MT", "Mato Grosso");
        public static State MatoGrossoSul = new State ("MS", "Mato Grosso Sul");
        public static State MinasGerais = new State ("MG", "Minas Gerais");
        public static State Para = new State ("PA", "Pará");
        public static State Paraiba = new State ("PB", "Paraíba");
        public static State Parana = new State ("PR", "Paraná");
        public static State Pernambuco = new State ("PE", "Pernambuco");
        public static State Piaui = new State ("PI", "Piauí");
        public static State RioDeJaneiro = new State ("RJ", "Rio de Janeiro");
        public static State RioGrandeDoNorte = new State ("RN", "Rio Grande do Norte");
        public static State RioGrandeDoSul = new State ("RS", "Rio Grande do Sul");
        public static State Rondonia = new State ("RO", "Rondônia");
        public static State Roraima = new State ("RR", "Roraima");
        public static State SantaCatarina = new State ("SC", "Santa Catarina");
        public static State SaoPaulo = new State ("SP", "São Paulo");
        public static State Sergipe = new State ("SE", "Sergipe");
        public static State Tocantis = new State ("TO", "Tocantis");

        #endregion

        #region Properties

        public string Name { get; private set; }
        public string Initials { get; private set; }

        #endregion

        #region Constructors

        private State () { }

        private State (string initials, string name) {
            Initials = initials;
            Name = name;
        }

        #endregion

        #region Methods

        public static IEnumerable<State> GetAll () {

            var fields = typeof (State).GetFields (BindingFlags.Public |
                BindingFlags.Static |
                BindingFlags.DeclaredOnly);

            return fields.Select (f => f.GetValue (null)).Cast<State> ();
        }

        public override string ToString () => Name;

        public override bool Equals (object obj) {
            var otherValue = obj as State;

            if (otherValue == null)
                return false;

            var typeMatches = GetType ().Equals (obj.GetType ());
            var valueMatches = Initials.Equals (otherValue.Initials);

            return typeMatches && valueMatches;
        }

        public int CompareTo (object other) => Initials.CompareTo (((State) other).Initials);

        public override int GetHashCode () {

            int hashCode = -1470874938;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode (Initials);
            return hashCode;

        }

        #endregion
    }

}