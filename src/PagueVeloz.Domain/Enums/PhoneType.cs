namespace PagueVeloz.Domain.Enums {
    public enum PhoneType {
        Commercial = 1,
        Cellphone = 2,
        Landline = 3
    }
}