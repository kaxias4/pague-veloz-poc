namespace PagueVeloz.Domain.Extensions {
    public static class StringExtensions {

        public static string GetOnlyNumbers (this string str) {

            string result = "";

            foreach (var c in str.ToCharArray ()) {
                if (char.IsNumber (c))
                    result += c;
            }

            return result;
        }

    }
}