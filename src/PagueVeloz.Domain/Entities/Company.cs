using System;
using System.Linq;
using PagueVeloz.Domain.Enums;
using PagueVeloz.Domain.Extensions;
using PagueVeloz.Domain.Interfaces;

namespace PagueVeloz.Domain.Entities {
    public class Company : IEntity {

        #region Properties

        public int Id { get; private set; }
        public string TradingName { get; private set; }
        public string CNPJ { get; private set; }
        public string UF { get; private set; }

        #endregion

        #region Constructors

        private Company () { }

        public Company (string tradingName, string registeredNumber,
            string uf) : base () {

            ChangeTrandigName (tradingName);
            ChangeCNPJ (registeredNumber);
            ChangeUf (uf);

        }

        #endregion

        #region Methods

        public void ChangeTrandigName (string tradingName) {

            if (string.IsNullOrWhiteSpace (tradingName))
                throw new ArgumentNullException (
                    "Nome fantasia é obrigatório.");

            TradingName = tradingName;
        }

        public void ChangeCNPJ (string cnpj) {

            if (string.IsNullOrWhiteSpace (cnpj))
                throw new ArgumentNullException ("CNPJ é obrigatório.");

            cnpj = cnpj.GetOnlyNumbers ();

            if (!cnpj.IsCnpj ())
                throw new ArgumentException ("CNPJ não é valido.");

            CNPJ = cnpj;
        }

        public void ChangeUf (string uf) {

            if (string.IsNullOrWhiteSpace (uf))
                throw new ArgumentNullException ("UF é obrigatório.");

            if (!State.GetAll ().Any (s =>
                    s.Initials.Equals (uf.ToUpper ())))
                throw new ArgumentException ("UF não é valido.");

            UF = uf;
        }

        #endregion

    }
}