using System;
using System.Collections.Generic;
using PagueVeloz.Domain.Extensions;

namespace PagueVeloz.Domain.Entities {
    public class LegalSupplier : Supplier {

        #region Properties

        public string CNPJ { get; private set; }

        #endregion

        #region Constructors

        private LegalSupplier () { }

        public LegalSupplier (
            string name,
            string cnpj,
            Company company,
            List<Phone> phones = null) : base (name, company, phones) {

            ChangeCNPJ (cnpj);

        }

        #endregion

        #region Methods

        public void ChangeCNPJ (string cnpj) {

            if (string.IsNullOrWhiteSpace (cnpj))
                throw new ArgumentNullException ("CNPJ é obrigatório.");

            cnpj = cnpj.GetOnlyNumbers ();

            if (!cnpj.IsCnpj ())
                throw new ArgumentException ("CNPJ não é valido.");

            CNPJ = cnpj;
        }

        #endregion
    }
}