using System;
using PagueVeloz.Domain.Enums;
using PagueVeloz.Domain.Interfaces;

namespace PagueVeloz.Domain.Entities
{
    public class Phone : IEntity
    {

        #region Properties

        public int Id { get; private set; }
        public string Number { get; private set; }
        public PhoneType Type { get; private set; }

        #endregion

        #region Constructors

        public Phone(string number, PhoneType type)
        {

            ChangeNumber(number);
            ChangePhoneType(type);

        }

        #endregion

        #region Methods

        public void ChangeNumber(string number)
        {

            if (string.IsNullOrWhiteSpace(number))
                throw new ArgumentNullException("Número fantasia é obrigatório.");

            Number = number;
        }

        public void ChangePhoneType(PhoneType type)
        {

            if (!Enum.IsDefined(Type.GetType(), type))
                throw new ArgumentNullException("Tipo de telefone inválido.");

            Type = type;
        }

        #endregion

    }
}