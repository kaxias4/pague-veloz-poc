using System;
using System.Collections.Generic;
using PagueVeloz.Domain.Extensions;
using PagueVeloz.Domain.Interfaces;

namespace PagueVeloz.Domain.Entities {
    public class IndividualSupplier : Supplier, IEntity {

        #region Properties

        public string CPF { get; private set; }
        public string RG { get; private set; }
        public DateTime DateOfBirth { get; set; }

        #endregion

        #region Constructors

        private IndividualSupplier () { }

        public IndividualSupplier (
            string name,
            string cpf,
            string rg,
            DateTime dateOfBirth,
            Company company,
            List<Phone> phones = null) : base (name, company, phones) {

            ChangeCPF (cpf);
            ChangeRG (rg);
            DateOfBirth = dateOfBirth;

        }

        #endregion

        #region Methods

        public void ChangeCPF (string cpf) {

            if (string.IsNullOrWhiteSpace (cpf))
                throw new ArgumentNullException ("CPF é obrigatório.");

            cpf = cpf.GetOnlyNumbers ();

            if (!cpf.IsCpf ())
                throw new ArgumentException ("CPF não é valido.");

            CPF = cpf;
        }

        public void ChangeRG (string rg) {

            rg = rg.GetOnlyNumbers ();

            if (string.IsNullOrWhiteSpace (rg))
                throw new ArgumentNullException ("Rg é obrigatório.");

            RG = rg;
        }

        #endregion
    }
}