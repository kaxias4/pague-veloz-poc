using System;
using System.Collections.Generic;
using PagueVeloz.Domain.Interfaces;

namespace PagueVeloz.Domain.Entities {
    public abstract class Supplier : IEntity {

        #region Properties

        public int Id { get; private set; }
        public string Name { get; private set; }
        public Company Company { get; private set; }
        public DateTime RegistrationDate { get; private set; }

        private List<Phone> _phones;
        public IReadOnlyCollection<Phone> Phones => _phones;

        #endregion

        #region Constructor

        protected Supplier () { }

        public Supplier (string name, Company company, List<Phone> phones = null) {

            ChangeName (name);
            ChangeCompany (company);

            _phones = new List<Phone> ();
            if (phones != null)
                phones.ForEach (p => AddPhone (p));

            RegistrationDate = DateTime.UtcNow;
        }

        #endregion

        #region Methods

        public void ChangeCompany (Company company) {

            if (company == null)
                throw new ArgumentNullException ("Empresa é obrigatório.");

            Company = company;
        }

        public void ChangeName (string name) {

            if (string.IsNullOrWhiteSpace (name))
                throw new ArgumentNullException (
                    "Nome fantasia é obrigatório.");

            Name = name;
        }

        public void AddPhone (Phone phone) {

            if (phone == null)
                throw new ArgumentNullException ("Telefone não pode ser vazio.");

            _phones.Add (phone);
        }

        public void RemovePhone (Phone phone) => _phones.Remove (phone);

        #endregion

    }
}