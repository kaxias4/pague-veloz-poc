using System;
using System.Linq;
using PagueVeloz.Domain.Entities;
using PagueVeloz.Domain.Interfaces.Infra.Data;
using PagueVeloz.Domain.Interfaces.Services;

namespace PagueVeloz.Domain.Services
{
    public class LegalSupplierService : Service<LegalSupplier>, ILegalSupplierService
    {
        public LegalSupplierService(IUnitOfWork uow) : base(uow)
        {
        }

        public override void Add(LegalSupplier entity)
        {

            var alreadyExist = _uow.GetRepository<LegalSupplier>()
                .GetListAsync(s => s.CNPJ == entity.CNPJ)
                .Result
                .Any();

            if (alreadyExist)
                throw new Exception("Fornecedor já cadastrado.");

            base.Add(entity);
        }

        public override void Update(LegalSupplier entity)
        {
            var alreadyExist = _uow.GetRepository<LegalSupplier>()
                .GetListAsync(s => s.CNPJ == entity.CNPJ && s.Id != entity.Id)
                .Result
                .Any();

            if (alreadyExist)
                throw new Exception("Fornecedor já cadastrado.");

            base.Update(entity);
        }

    }
}