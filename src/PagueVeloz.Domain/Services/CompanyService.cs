using System;
using System.Linq;
using PagueVeloz.Domain.Entities;
using PagueVeloz.Domain.Interfaces.Infra.Data;
using PagueVeloz.Domain.Interfaces.Services;

namespace PagueVeloz.Domain.Services
{
    public class CompanyService : Service<Company>, ICompanyService
    {
        public CompanyService(IUnitOfWork uow) : base(uow)
        {

        }

        public override void Add(Company entity)
        {

            var alreadyExist = base.GetListAsync(c => c.CNPJ == entity.CNPJ)
                .Result
                .Any();

            if (alreadyExist)
                throw new Exception("Empresa já cadastrada.");

            base.Add(entity);
        }

        public override void Update(Company entity)
        {
            var alreadyExist = base.GetListAsync(c => c.CNPJ == entity.CNPJ && c.Id != entity.Id)
                .Result
                .Any();

            if (alreadyExist)
                throw new Exception("Empresa já cadastrada.");

            base.Update(entity);
        }
    }
}