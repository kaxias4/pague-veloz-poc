using System;
using System.Linq;
using PagueVeloz.Domain.Entities;
using PagueVeloz.Domain.Enums;
using PagueVeloz.Domain.Interfaces.Infra.Data;
using PagueVeloz.Domain.Interfaces.Services;

namespace PagueVeloz.Domain.Services
{
    public class IndividualSupplierService : Service<IndividualSupplier>, IIndividualSupplierService
    {
        public IndividualSupplierService(IUnitOfWork uow) : base(uow)
        {
        }

        public override void Add(IndividualSupplier entity)
        {

            var alreadyExist = _uow.GetRepository<IndividualSupplier>()
                .GetListAsync(s => s.CPF == entity.CPF)
                .Result
                .Any();

            if (alreadyExist)
                throw new Exception("Fornecedor já cadastrado.");

            if (entity.Company.UF.Equals(State.Parana.Initials))
            {
                int age = 0;
                age = DateTime.Now.Year - entity.DateOfBirth.Year;
                if (DateTime.Now.DayOfYear < entity.DateOfBirth.DayOfYear)
                    age = age - 1;

                if (age < 18)
                    throw new ArgumentException("Fornecedores pessoa fisica do parana precisar ser maiores de idade.");
            }

            base.Add(entity);
        }

        public override void Update(IndividualSupplier entity)
        {
            var alreadyExist = _uow.GetRepository<IndividualSupplier>()
                .GetListAsync(s => s.CPF == entity.CPF && s.Id != entity.Id)
                .Result
                .Any();

            if (alreadyExist)
                throw new Exception("Fornecedor já cadastrado.");

            if (entity.Company.UF.Equals(State.Parana.Initials))
            {
                int age = 0;
                age = DateTime.Now.Year - entity.DateOfBirth.Year;
                if (DateTime.Now.DayOfYear < entity.DateOfBirth.DayOfYear)
                    age = age - 1;

                if (age < 18)
                    throw new ArgumentException("Fornecedores pessoa física do parana precisar ser maiores de idade.");
            }

            base.Update(entity);
        }

    }
}