using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PagueVeloz.Domain.Interfaces;
using PagueVeloz.Domain.Interfaces.Infra.Data;
using PagueVeloz.Domain.Interfaces.Services;

namespace PagueVeloz.Domain.Services
{
    public class Service<TEntity> : IService<TEntity> where TEntity : class
    {

        #region Properties

        protected readonly IUnitOfWork _uow;

        #endregion

        #region Constructors

        public Service(IUnitOfWork uow) => _uow = uow;

        #endregion

        #region Methods

        public async Task<IList<TEntity>> GetListAsync(
            Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> include = null,
            int index = 0, int size = 20, bool disableTracking = true)
        {

            return await _uow.GetRepository<TEntity>()
                .GetListAsync(predicate, orderBy, include, index, size, disableTracking);
        }

        public async Task<TEntity> SingleAsync(
            Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> include = null,
            bool disableTracking = true)
        {

            return await _uow.GetRepository<TEntity>()
                .SingleAsync(predicate, orderBy, include, disableTracking);

        }

        public virtual void Add(TEntity entity)
        {
            _uow.GetRepository<TEntity>().Add(entity);
            _uow.SaveChanges();
        }


        public virtual void Update(TEntity entity)
        {
            _uow.GetRepository<TEntity>().Update(entity);
            _uow.SaveChanges();
        }


        public void Delete(TEntity entity)
        {
            _uow.GetRepository<TEntity>().Delete(entity);
            _uow.SaveChanges();
        }

        #endregion
    }
}